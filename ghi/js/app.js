function createCard(name, location, description, pictureUrl, formattedDate, formattedEnding) {
  return `
    <div class="card shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2"><small class="text-muted">${location}</small></h6>
        <p class="card-text">${description}</p>
        <p class="card-text">Starts: ${formattedDate}</p>
        <p class="card-text">Ends: ${formattedEnding}</p>
      </div>
    </div>
    <div class="alert alert-danger" role="alert">
     A simple danger alert—check it out!
    ß</div>
  `;

}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error('Error: Bad response from the server');
    } else {
      const data = await response.json();

      let columnIndex = 0;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const location = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const formattedDate = startDate.toLocaleDateString('en-US', {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',

          });
          const endDate = new Date(details.conference.ends)
          const formattedEnding = endDate.toLocaleDateString('en-US', {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
          });


          const html = createCard(name, location, description, pictureUrl, formattedDate, formattedEnding);

          const column = document.querySelectorAll('.col')[columnIndex];
          column.innerHTML += html;

          columnIndex++;

          if (columnIndex >= 3) {
            columnIndex = 0;
          }
        }
      }
    }
  } catch (e) {
    console.error('An error occurred:', e);
  }
});
