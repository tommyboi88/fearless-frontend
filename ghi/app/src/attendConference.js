import React, { useEffect, useState } from 'react';

function AttendConferenceForm(props) {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [conference, setConference] = useState("");
  const [conferences, setConferences] = useState([]);
  const [loading, setLoading] = useState(true);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name: name,
      email: email,
      conference: conference,
    }

    console.log(data);

    const attendeesUrl = `http://localhost:8001${data.conference}attendees/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(attendeesUrl, fetchConfig);
    if (response.ok) {
      const newAttendeesConference = await response.json();
      console.log(newAttendeesConference);
      setName("");
      setEmail("");
      setConference("");
    }
  }

  const handleNameChange = (event) => setName(event.target.value);
  const handleEmailChange = (event) => setEmail(event.target.value);
  const handleConferenceChange = (event) => setConference(event.target.value);

  useEffect(() => {
    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';

      try {
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
          setLoading(false);
        } else {
          throw new Error('Failed to fetch data');
        }
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="col col-sm-auto">
        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="./images/logo.svg" />
      </div>
      <div className="col">
        <div className="card shadow">
          <div className="card-body">
            <form onSubmit={handleSubmit} id="create-attendee-form">
              <h1 className="card-title">It's Conference Time!</h1>
              <p className="mb-3">
                Please choose which conference
                you'd like to attend.
              </p>
              <div className={`d-flex justify-content-center mb-3 ${loading ? '' : 'd-none'}`} id="loading-conference-spinner">
                <div className="spinner-grow text-secondary" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              </div>
              <div className={`mb-3 ${loading ? 'd-none' : ''}`}>
                <select required name="conference" id="conference" className="form-select" onChange={handleConferenceChange}>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => (
                    <option key={conference.href} value={conference.href}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>
              <p className="mb-3">Now, tell us about yourself.</p>
              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input required placeholder="Your full name" type="text" id="name" name="name" className="form-control" onChange={handleNameChange} value={name} />
                    <label htmlFor="name">Your full name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input required placeholder="Your email address" type="email" id="email" name="email" className="form-control" onChange={handleEmailChange} value={email} />
                    <label htmlFor="email">Your email address</label>
                  </div>
                </div>
              </div>
              <button className="btn btn-lg btn-primary">I'm going!</button>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AttendConferenceForm;
