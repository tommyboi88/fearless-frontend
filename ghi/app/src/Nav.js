import React from 'react';
import './Nav.css';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-white">
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link" aria-current="page" href="#">
              Conference GO!
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" aria-current="page" href="#">
              Home
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link nav-link-grey" href="new-location.html">
              New location
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link nav-link-grey" href="new-conference.html">
              New conference
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link nav-link-grey" href="new-presentation.html">
              New presentation
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Nav;
