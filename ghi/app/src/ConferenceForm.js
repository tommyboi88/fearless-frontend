import React , { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [name, setName] = useState(" ");
    const currentDate = new Date().toISOString().split('T')[0];
    const [startDate, setStartDate] = useState(currentDate);
    const [endDate, setEndDate] = useState(currentDate);
    const [description, setDescription] = useState(" ");
    const [maxPresentations, setMaxPresentations] = useState(0);
    const [maxAttendees, setMaxAttendees] = useState(0);
    const [location, setLocation] = useState(" ");
    const [locations, setLocations] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName(' ');
            setStartDate(currentDate);
            setEndDate(currentDate);
            setDescription(' ');
            setMaxPresentations(0);
            setMaxAttendees(0);
            setLocation(' ');
        }
    }


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }

    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8000/api/locations/';

            try {
                const response = await fetch(url);

                if (response.ok) {
                    const data = await response.json();
                    setLocations(data.locations);
                } else {
                    throw new Error("Failed to fetch data");
                }
            }  catch (error) {
                console.error(error);
            }
    }
    fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input placeholder="Name" required type="text" name="name" id="name" className="form-control"  onChange={handleNameChange} value={name} />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"  onChange={handleStartDateChange} value={startDate} />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"  onChange={handleEndDateChange} value={endDate} />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea placeholder="Description" name="description" id="description" className="form-control" rows="3"  onChange={handleDescriptionChange} value={description}></textarea>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"  onChange={handleMaxPresentationsChange} value={maxPresentations} />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"  onChange={handleMaxAttendeesChange} value={maxAttendees} />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select required name="location" id="location" className="form-select" onChange={handleLocationChange}>
                  <option value={location}>Choose a Location</option>

                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
